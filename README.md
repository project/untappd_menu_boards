INTRODUCTION
------------

This module provides configurable widgets that allow you to place Untappd menus 
on your site from the API at https://business.untappd.com

This module is a plugin for the [Bean Module](https://drupal
.org/project/bean) which creates configurable pieces of content (similar to 
nodes) which can then placed in various locations around your site (like 
blocks!)

REQUIREMENTS
------------

- Drupal Core 7.x
- [Bean Module](https://drupal.org/project/bean) 7.x-1.x
- Untappd Business API Access (https://business.untappd.com)

INSTALLATION
------------

Download and install this module as you would any other Drupal module. Once 
uploaded, you can enable this module on your `admin/modules` page. For 
detailed instructions on installing contributed modules see: 
<https://drupal.org/documentation/install/modules-themes/modules-7>

CONFIGURATION
-------------

First, set up user permissions for CRUD actions at `/admin/people/permissions`.

Then, create a new block by visiting the `/block/add/untappd-menu-boards` 
section of your website. Once saved, the blocks can be placed in regions of 
your theme using the `/admin/structure/blocks` interface. For more info on 
the core block module see: 
<https://www.drupal.org/docs/7/core/modules/block/overview>

Theming
-------

The appearance of the menu can be configured on a per-bean basis with settings 
on the form:

- Display Prices
- Display "On Deck" section

Templates
---------

The module provides a default `untappd-menu-boards.tpl.php` file which will 
adjust the appearance of all Untappd Menu Boards blocks.

You can customize specific blocks by overriding the global template:
`untappd-menu-boards--menu--[menu_id].tpl.php` where menu_id is the API id 
for the block you wish to override.

If you want all blocks for one location to be styled the same you can 
override based on location as well: 
`untappd-menu-boards--location--[location_id].tpl.php` where location_id is the
API id for the location that contains the block.
