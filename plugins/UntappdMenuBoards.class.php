<?php

/**
 * @file
 * Defines a Bean which queries for menus from the Untappd API.
 */

/**
 * Creates a custom Bean object that utilizes the Untappd Business API.
 */
class UntappdMenuBoards extends BeanPlugin {

  /**
   * Declares default block settings.
   *
   * @return array
   *   Array of default settings.
   */
  public function values() {
    return [
      'menu_options' => [
        'menu_id' => '',
        'cache_time' => 0,
      ],
      'display_options' => [
        'show_on_deck' => FALSE,
        'show_prices' => FALSE,
      ],
    ];
  }

  /**
   * Builds extra settings for the block edit form.
   */
  public function form($bean, $form, &$form_state) {

    if (!(variable_get('untappd_menu_boards_api_key', FALSE) || variable_get('untappd_menu_boards_api_email', FALSE))) {
      drupal_set_message($this->t('You need to configure the <a href="@untappd-menu-boards-config" target="_blank">Untappd API Access Credentials</a> before creating new blocks.', ['@untappd-menu-boards-config' => url('admin/config/services/untappd_menu_boards')]), 'error');
      return [];
    }

    $form = [];

    $form['menu_options'] = [
      '#type' => 'fieldset',
      '#tree' => TRUE,
      '#title' => $this->t('Menu Options'),
      '#description' => $this->t('Configuration options for the request to the Untappd API'),
    ];

    $form['menu_options']['menu_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Menu ID'),
      '#size' => 10,
      '#default_value' => $bean->menu_options['menu_id'],
      '#description' => $this->t('The Menu ID of the Untappd Menu you would like to view. You can easily find this value in your Untappd for Business account (example: https://business.untappd.com/venues/xxx/builder/menus/12345 -- 12345 is the menu_id)'),
      '#required' => TRUE,
    ];

    $form['menu_options']['cache_time'] = [
      '#type' => 'select',
      '#title' => $this->t('Cache Request Expiration'),
      '#options' => [
        0 => $this->t('Do not cache requests'),
        900 => $this->t('15 minutes'),
        1800 => $this->t('30 minutes'),
        3600 => $this->t('1 hour'),
        7200 => $this->t('2 hours'),
        14400 => $this->t('4 hours'),
        28800 => $this->t('8 hours'),
      ],
      '#default_value' => $bean->menu_options['cache_time'],
      '#description' => $this->t('How long should results from the API be cached? (Recommended to avoid hitting the <a href="@untappd-api-docs" target="_blank">API limit</a>)', ['@untappd-api-docs' => "https://untappd.com/api/docs"]),
      '#required' => TRUE,
    ];

    $form['display_options'] = [
      '#type' => 'fieldset',
      '#tree' => TRUE,
      '#title' => $this->t('Display Options'),
      '#description' => $this->t('Menu Display Options'),
    ];

    $form['display_options']['show_on_deck'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show "On Deck" Section?'),
      '#default_value' => $bean->display_options['show_on_deck'],
      '#description' => $this->t('Whether to show the upcoming "on deck" section in this menu (if defined).'),
    ];

    $form['display_options']['show_prices'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Display Prices?'),
      '#default_value' => $bean->display_options['show_prices'],
      '#description' => $this->t('Whether to show prices on the menu.'),
    ];

    return $form;
  }

  /**
   * Displays the bean.
   */
  public function view($bean, $content, $view_mode = 'default', $langcode = NULL) {
    $menu_id = $bean->menu_options['menu_id'];

    $result = _untappd_menu_boards_build_request("/menus/" . $menu_id, ['full' => TRUE], $bean->menu_options['cache_time']);

    if (!is_object($result)) :
      return [];
    endif;

    // Build menu render array.
    $sections = [];
    foreach ($result->menu->sections as $section) {
      $items = [];
      foreach ($section->items as $item) {
        $prices = [];
        if ($bean->display_options['show_prices']) {
          // Menu should display prices so populate pricing array.
          foreach ($item->containers as $container) {
            $prices[] = [
              'size' => $container->container_size->name,
              'price' => $container->price,
            ];
          }
        }
        $items[] = [
          'tap_number' => $item->tap_number,
          'name' => $item->name,
          'style' => $item->style,
          'abv' => $item->abv,
          'calories' => $item->calories,
          'prices' => $prices,
        ];
      }
      $sections[] = [
        'name' => $section->name,
        'items' => $items,
      ];
    }

    if ($bean->display_options['show_on_deck']) {
      $on_deck = $result->menu->on_deck_section;
      $items = [];
      foreach ($on_deck->items as $item) {
        $prices = [];
        if ($bean->display_options['show_prices']) {
          // Menu should display prices so populate pricing array.
          foreach ($item->containers as $container) {
            $prices[] = [
              'size' => $container->container_size->name,
              'price' => money_format("%n", $container->price),
            ];
          }
        }
        $items[] = [
          'tap_number' => $item->tap_number,
          'name' => $item->name,
          'style' => $item->style,
          'abv' => $item->abv,
          'calories' => $item->calories,
          'prices' => $prices,
        ];
      }
      $sections['on_deck'] = [
        'name' => $on_deck->name,
        'items' => $items,
      ];
    }

    // Make sure content from the API does not contain anything suspicious.
    array_walk_recursive($sections, "_untappd_menu_boards_api_filter_xss");

    return [
      '#theme' => 'untappd_menu_boards',
      '#menu_id' => $result->menu->id,
      '#location_id' => $result->menu->location_id,
      '#menu_sections' => $sections,
      '#last_updated' => strtotime($result->menu->updated_at),
    ];
  }

}
