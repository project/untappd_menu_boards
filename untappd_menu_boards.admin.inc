<?php

/**
 * @file
 * Admin form functions for Untappd Menu Boards Module.
 */

/**
 * Implements hook_form().
 *
 * @see untappd_menu_boards_menu()
 */
function untappd_menu_boards_settings_form($form, &$form_state) {
  $form = [];

  $form['untappd_menu_boards_api_key'] = [
    '#type' => 'textfield',
    '#title' => t('API key'),
    '#required' => TRUE,
    '#default_value' => variable_get('untappd_menu_boards_api_key', FALSE),
    '#description' => t('Untappd API Token. <a href="@untappd-api-tokens" target="_blank">More Information</a>', ['@untappd-api-tokens' => "https://business.untappd.com/api_tokens"]),
  ];

  $form['untappd_menu_boards_api_email'] = [
    '#type' => 'textfield',
    '#title' => t('Email Address'),
    '#required' => TRUE,
    '#default_value' => variable_get('untappd_menu_boards_api_email', FALSE),
    '#description' => t('Untappd Account Email Address for use with the provided API token.'),
  ];

  return system_settings_form($form);
}

/**
 * Implements hook_form_validate().
 *
 * Attempts authentication with provided credentials to validate input.
 *
 * @see https://docs.business.untappd.com/#authentication
 */
function untappd_menu_boards_settings_form_validate($form, &$form_state) {
  $token = $form_state['values']['untappd_menu_boards_api_key'];
  $email = $form_state['values']['untappd_menu_boards_api_email'];

  $encoded_auth = _untappd_menu_boards_generate_auth($token, $email);

  // Set authorization header.
  $options = [
    'headers' => [
      'Authorization' => 'Basic ' . $encoded_auth,
    ],
  ];

  // Get result.
  $result = drupal_http_request(UNTAPPD_MENU_BOARDS_ENDPOINT . '/current_user', $options);
  if (property_exists($result, 'error')) {
    form_set_error('untappd_menu_boards_api_key', t('There was a problem authenticating with the provided credentials. @untappd-api-error', ['@untappd-api-error', $result->error]));
  }
  else {
    drupal_set_message(t("Successfully Connected to Untappd API"), "status", FALSE);
  }

}
