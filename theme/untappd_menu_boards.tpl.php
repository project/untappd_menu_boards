<?php

/**
 * @file
 * Theme template for Untappd Menu Board.
 *
 * Available Variables
 * - $menu_sections array - Condensed menu data result from API
 * Example Menu Array
 * - menu_sections array
 *   - name string The beer name
 *   - style string Beer style
 *   - abv int ABV
 *   - calories int Calories
 *   - prices array Defined Container Sizes
 *     - price string Price of this container
 *     - size string Name of this Container
 * - $last_updated timestamp - When was this menu last updated?
 * - $wrapper string - Wrapper tag to use.
 *
 * Helper variables:
 * - $classes_array: Array of html class attribute values. It is flattened
 * into a string within the variable $classes.
 * - $is_front: Flags true when presented in the front page.
 * - $logged_in: Flags true when the current user is a logged-in member.
 * - $is_admin: Flags true when the current user is an administrator.
 *
 * @see \untappd_menu_boards_preprocess_untappd_menu_boards()
 */
?>

<div class="container <?php echo $classes; ?>">
  <?php foreach($menu_sections as $section) { ?>
    <div class="untappd-menu-section">
      <h2 class='untappd-menu-title'><?php echo $section['name']; ?></h2>
      <ul>
      <?php foreach($section['items'] as $item) { ?>
        <li class="untappd-item">
          <span class="untappd-item-name"><strong><?php echo $item['name']; ?></strong></span>
          <span class="untappd-item-style"><?php echo $item['style']; ?></span>
          <span class="untappd-item-abv"><?php echo t('ABV: @abv', ['@abv' => $item['abv']]); ?></span>
          <span class="untappd-item-calories"><?php echo t('Calories: @cals', ['@cals' => $item['calories']]); ?></span>
          <?php foreach($item['prices'] as $price) {
            // This will be an empty array if they did not select to display
            // prices.
          ?>
            <div class="untappd-item-container">
              <span class="untappd-item-container-size"><?php echo $price['size']; ?></span>
              <span class="untappd-item-container-price"><?php echo $price['price']; ?></span>
            </div>
          <?php } ?>
        </li>
      <?php } ?>
      </ul>
    </div>
  <?php } ?>
  <small><strong>Last Updated: <?php echo format_date($last_updated, 'medium'); ?></strong></small>
</div>
