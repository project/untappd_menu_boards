<?php

/**
 * @file
 * Theme pre-process file for the Untappd Menu Boards module.
 */

/**
 * Implements hook_preprocess_hook().
 */
function untappd_menu_boards_preprocess_untappd_menu_boards(&$variables) {
  // Add override for template by location ID.
  $variables['theme_hook_suggestions'][] = 'untappd_menu_boards__location__' . $variables['location_id'];
  $variables['classes_array'][] = 'untappd-menu-boards-' . $variables['location_id'];
  // Add override for template by menu ID.
  $variables['theme_hook_suggestions'][] = 'untappd_menu_boards__menu__' . $variables['menu_id'];
  $variables['classes_array'][] = 'untappd-menu-boards-' . $variables['menu_id'];
}
